image: Visual Studio 2019

environment:
  # Statically link the Windows 10 Universal CRT, so that it is not a runtime dependency
  RUSTFLAGS: "-C target-feature=+crt-static"
  RUST_BACKTRACE: "1"

install:
  # Install 32-bit versions of LLVM, GraalVM and JDK11
  - cinst llvm --x86
  - cinst graalvm --version 20.3.0
  - cinst adoptopenjdk11 --x86 --params="/ADDLOCAL=FeatureMain,FeatureEnvironment,FeatureJarFileRunWith,FeatureJavaHome /INSTALLDIR=%ProgramFiles%\AdoptOpenJDK"
  # Set GraalVM home variable separately
  - ps: Set-Item -Path env:GRAALVM_HOME -Value ($env:ProgramFiles + "\GraalVM\graalvm-ce-java11-20.3.0")
  # Refresh environment variables to ensure JAVA_HOME has been updated
  - cmd: refreshenv
  # Fetch and install Rust tooling
  - ps: Invoke-WebRequest -Uri https://win.rustup.rs/i686 -OutFile rustup-init.exe
  - cmd: .\rustup-init.exe -y --profile minimal --default-toolchain stable --default-host i686-pc-windows-msvc
  # Add cargo build tool and Inno Setup to PATH
  - ps: Set-Item -Path env:PATH -Value ($env:PATH + ";" + $env:USERPROFILE + "\.cargo\bin")
  - ps: Set-Item -Path env:PATH -Value ($env:PATH + ";" + "C:\Program Files (x86)\Inno Setup 6")
  # Add 64-bit MSVC Rust toolchain for repacker
  - cmd: rustup toolchain install stable-x86_64-pc-windows-msvc
  - cmd: rustup target add x86_64-pc-windows-msvc
  # Set release version
  - ps: Set-Item -Path env:RELEASE_VERSION -Value $(git describe --tags)

build_script:
  # Launcher executable
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%\launcher
  - cmd: cargo build
  - cmd: cargo build --release

  # PhysFS JNI DLL
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%\physfs_jni
  - cmd: cargo build
  - cmd: cargo build --release

  # PhysFS RTS DLL
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%\physfs_rts
  - cmd: cargo build
  - cmd: cargo build --release

  # SFS repacker
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%\repacker
  - cmd: cargo build
  - cmd: cargo build --release

  # Java PhysFS library
  #
  # Copy the PhysFS JNI DLL to the Java build directory so that we ensure the latest version is tested
  - cmd: copy /y %APPVEYOR_BUILD_FOLDER%\physfs_jni\target\release\physfs_jni.dll %APPVEYOR_BUILD_FOLDER%\physfs_java
  - cmd: cd %APPVEYOR_BUILD_FOLDER%
  - cmd: .\gradlew.bat :physfs_java:jar

  # Class loading agent
  #
  # Set up C++ compiler environment for native-image
  - cmd: call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"
  - cmd: cd %APPVEYOR_BUILD_FOLDER%
  - cmd: .\gradlew.bat :classload_agent:jar
  - cmd: .\gradlew.bat :classload_agent:nativeImage
  # Restore 32-bit compiler environment
  - cmd: call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars32.bat"

  # Custom JRE with JMX debugging support
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%
  - ps: jlink --no-header-files --no-man-pages --add-modules java.base,java.instrument,java.desktop,jdk.localedata,jdk.jdwp.agent,jdk.management.agent --output jre_debug

  # Custom JRE
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%
  - ps: jlink --no-header-files --no-man-pages --add-modules java.base,java.instrument,java.desktop,jdk.localedata --output jre

  # Inno Setup installer
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%
  - cmd: iscc /O%APPVEYOR_BUILD_FOLDER%\installer\output %APPVEYOR_BUILD_FOLDER%\installer\openil2-debug.iss
  - cmd: iscc /O%APPVEYOR_BUILD_FOLDER%\installer\output %APPVEYOR_BUILD_FOLDER%\installer\openil2.iss

test_script:
  # Launcher executable
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%\launcher
  - cmd: cargo test

  # PhysFS JNI DLL
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%\physfs_jni
  - cmd: cargo test

  # PhysFS RTS DLL
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%\physfs_rts
  - cmd: cargo test

  # Java PhysFS library
  #
  - cmd: cd %APPVEYOR_BUILD_FOLDER%
  - cmd: .\gradlew.bat :physfs_java:test
  - cmd: .\gradlew.bat :physfs_java:jacocoTestReport

after_test:
  # Stop the Gradle daemon so it doesn't lock files in the build cache
  - cmd: cd %APPVEYOR_BUILD_FOLDER%
  - cmd: .\gradlew.bat --stop

artifacts:
  - path: installer\output\openil2-installer-debug.exe
    name: OpenIL2 Installer (Debug Build)

  - path: installer\output\openil2-installer.exe
    name: OpenIL2 Installer

  - path: launcher\target\release\openil2.exe
    name: OpenIL2 Launcher
  - path: launcher\target\debug\openil2.exe
    name: OpenIL2 Launcher (Debug Build)

  - path: repacker\target\release\repacker.exe
    name: SFS Repacker
  - path: repacker\target\debug\repacker.exe
    name: SFS Repacker (Debug Build)

  - path: physfs_jni\target\release\physfs_jni.dll
    name: PhysFS JNI DLL
  - path: physfs_jni\target\debug\physfs_jni.dll
    name: PhysFS JNI DLL (Debug Build)

  - path: physfs_rts\target\release\physfs_rts.dll
    name: PhysFS RTS DLL
  - path: physfs_rts\target\debug\physfs_rts.dll
    name: PhysFS RTS DLL (Debug Build)

  - path: physfs_java\build\libs\physfs_java.jar
    name: PhysFS Java JAR
  - path: physfs_java\build\reports\tests\test
    name: PhysFS Java JUnit Test Report
  - path: physfs_java\build\reports\jacoco\test
    name: PhysFS Java Jacoco Coverage Report

  - path: classload_agent\build\libs\classload_agent.jar
    name: Class Loading Agent JAR

  - path: classload_agent\build\executable\class-transformer.exe
    name: Class Transformer

deploy:
  - provider: BinTray
    username: davidgregory084
    api_key:
      secure: JKdWcEbXrEM9DuQfYeWkfYchP/Ybo1IKkCaJssc9bxYvroer3bGHfXLU2vK68Edi
    subject: openil2
    repo: OpenIL2
    package: OpenIL2
    version: $(RELEASE_VERSION)
    publish: true
    artifact: /.*openil2-installer.*\.exe/
    on:
      APPVEYOR_REPO_TAG: true

cache:
  - '%USERPROFILE%\.cargo'
  - '%USERPROFILE%\.gradle\caches'
  - '%USERPROFILE%\.gradle\wrapper'
